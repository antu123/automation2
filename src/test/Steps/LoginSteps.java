package Steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LoginSteps
{ WebDriver driver;

    @Given("^i opened the browser$")
    public void i_opened_the_browser() throws Throwable {
        System.setProperty("webdriver.gecko.driver","src/main/Drivers/geckodriver.exe");
        driver=new FirefoxDriver();
        driver.get("https://www.yupptv.com/");

    }

    @And("^i click on the login Link$")
    public void i_click_on_the_login_Link() throws Throwable {
        driver.findElement(By.partialLinkText("Sign in")).click();
    }

    @Then("^i should be able to enter the username and password$")
    public void i_should_be_able_to_enter_the_username_and_password() throws Throwable {
        driver.findElement(By.name("txtLogin")).sendKeys("chris_varghese123@yahoo.com");
        driver.findElement(By.name("txtpassword")).sendKeys("Pa55w0rd");
    }

    @When("^i click on the login button$")
    public void i_click_on_the_login_button() throws Throwable {
        driver.findElement(By.name("btnSubmit")).click();

    }

    @Then("^i should be able to login$")
    public void i_should_be_able_to_login() throws Throwable {
        String TitllePage = driver.getTitle();
        System.out.println(TitllePage);

    }


}
